from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def TodoList_list(request):
    todos = TodoList.objects.all()
    context = {"TodoList_list": todos}
    return render(request, "todos/list.html", context)


def show_TodoList(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {"todo_object": todo}
    return render(request, "todos/details.html", context)


def create_TodoList(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_TodoList(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.id)
    else:
        form = TodoListForm(instance=post)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_TodoList(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_TodoItem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/createitem.html", context)


def update_TodoItem(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "form": form,
    }
    return render(request, "todos/edititem.html", context)
