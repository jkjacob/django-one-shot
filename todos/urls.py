from django.urls import path
from todos.views import (
    TodoList_list,
    show_TodoList,
    create_TodoList,
    update_TodoList,
    delete_TodoList,
    create_TodoItem,
    update_TodoItem,
)

urlpatterns = [
    path("", TodoList_list, name="todo_list_list"),
    path("<int:id>/", show_TodoList, name="todo_list_detail"),
    path("create/", create_TodoList, name="todo_list_create"),
    path("<int:id>/edit/", update_TodoList, name="todo_list_update"),
    path("<int:id>/delete/", delete_TodoList, name="todo_list_delete"),
    path("items/create/", create_TodoItem, name="todo_item_create"),
    path("items/<int:id>/edit/", update_TodoItem, name="todo_item_update"),
]
